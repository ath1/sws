package de.thaso.sws.ws.db.manager;

import javax.enterprise.inject.Disposes;
import javax.enterprise.inject.Produces;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceContextType;

/**
 * EntityManagerProducer
 *
 * @author thaler
 * @since 2017-12-12
 */
public class EntityManagerProducer {

    @PersistenceContext(unitName = "flightsdb", type = PersistenceContextType.EXTENDED)
    private EntityManager entityManager;

    @Produces
    public EntityManager createEntityManager() {
        return entityManager;
    }

    protected void closeEntityManager(@Disposes EntityManager entityManager) {
        if (entityManager.isOpen()) {
            entityManager.close();
        }
    }
}
