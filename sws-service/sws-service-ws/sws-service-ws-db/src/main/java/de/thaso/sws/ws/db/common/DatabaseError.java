package de.thaso.sws.ws.db.common;

/**
 * DatabaseError
 *
 * @author thaler
 * @since 12.09.16
 */
public enum DatabaseError {
    COMMON_DATABASE,
    DATABASE_CONNECTION,
    ENTITY_NOT_FOUND
}
