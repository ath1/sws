package de.thaso.sws.ws.db;

import de.thaso.sws.ws.db.common.EntityBase;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Entity
@Table(name = "T_FLIGHT")
@NamedQueries({
        @NamedQuery(name = FlightEntity.FIND_BY_DATE, query = "select n from FlightEntity n where n.departure > :date"),
        @NamedQuery(name = FlightEntity.FIND_BY_AIRPORT, query = "select n from FlightEntity n where n.departure > :date and n.airport = :airport"),
        @NamedQuery(name = FlightEntity.FIND_BY_TARGET, query = "select n from FlightEntity n where n.departure > :date and n.airport = :airport and n.target = :target"),
})
public class FlightEntity extends EntityBase {

    private static final long serialVersionUID = 3365937645927953132L;

    public static final String FIND_BY_DATE = "FIND_BY_DATE";
    public static final String FIND_BY_AIRPORT = "FIND_BY_AIRPORT";
    public static final String FIND_BY_TARGET = "FIND_BY_TARGET";

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "FlightSequence")
    @SequenceGenerator(name = "FlightSequence", sequenceName = "SEQ_ID_FLIGHT", allocationSize = 10)
    @Column(name = "ID")
    private Long id;

    @Column(name = "FLYNO")
    @Size(min=2, max=6)
    @NotNull
    private String flyno;

    @Column(name = "DEPARTURE")
    @Temporal(TemporalType.TIMESTAMP)
    private String departure;

    @Column(name = "AIRPORT")
    @Size(max=25)
    private String airport;

    @Column(name = "TARGET")
    @Size(max=25)
    private String target;

    @Override
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFlyno() {
        return flyno;
    }

    public void setFlyno(String flyno) {
        this.flyno = flyno;
    }

    public String getDeparture() {
        return departure;
    }

    public void setDeparture(String departure) {
        this.departure = departure;
    }

    public String getAirport() {
        return airport;
    }

    public void setAirport(String airport) {
        this.airport = airport;
    }

    public String getTarget() {
        return target;
    }

    public void setTarget(String target) {
        this.target = target;
    }
}
