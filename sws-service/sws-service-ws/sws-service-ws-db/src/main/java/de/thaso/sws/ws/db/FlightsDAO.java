package de.thaso.sws.ws.db;

import de.thaso.sws.ws.db.common.DatabaseError;
import de.thaso.sws.ws.db.common.DatabaseException;
import org.apache.commons.lang3.time.DateUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class FlightsDAO {
    private final static Logger LOG = LoggerFactory.getLogger(FlightsDAO.class);

    @Inject
    private EntityManager entityManager;

    public FlightEntity storeFlight(final FlightEntity flightEntity) {
        LOG.info("storeFlight with id {} and flyno {}", flightEntity.getId(), flightEntity.getFlyno());

        entityManager.persist(flightEntity);
        return flightEntity;
    }

    public FlightEntity findFlightById(final Long id) {
        LOG.info("findFlightById {}", id);

        final FlightEntity flightEntity = entityManager.find(FlightEntity.class, id);
        return flightEntity;
    }

    public FlightEntity loadFlightById(final Long id) throws DatabaseException {
        LOG.info("loadFlightById {}", id);

        final FlightEntity flightEntity = entityManager.find(FlightEntity.class, id);
        if(flightEntity == null) {
            throw new DatabaseException(DatabaseError.ENTITY_NOT_FOUND, "Flight with id '" + id + "' not found!");
        }
        return flightEntity;
    }

    public FlightEntity findByFlyNo(final String flyno) {
        LOG.info("findByFlyNo( {} )", flyno);

        final TypedQuery<FlightEntity> query
                = entityManager.createNamedQuery(FlightEntity.FIND_BY_DATE, FlightEntity.class);
        query.setParameter("flyno", flyno);
        return query.getSingleResult();
    }

    public FlightEntity loadByFlyNo(final String flyno) {
        LOG.info("loadByFlyNo( {} )", flyno);

        final TypedQuery<FlightEntity> query
                = entityManager.createNamedQuery(FlightEntity.FIND_BY_DATE, FlightEntity.class);
        query.setParameter("flyno", flyno);
        FlightEntity singleResult = query.getSingleResult();
        if (singleResult == null) {
            throw new DatabaseException(DatabaseError.ENTITY_NOT_FOUND, "Flight with flyno '" + flyno + "' not found!");
        }
        return singleResult;
    }

    public List<FlightEntity> findByDate(final Date date) {
        LOG.info("findByDate( {} )", date);

        final TypedQuery<FlightEntity> query
                = entityManager.createNamedQuery(FlightEntity.FIND_BY_DATE, FlightEntity.class);
        query.setParameter("departure", DateUtils.ceiling(date, Calendar.DATE));
        query.setMaxResults(100);
        return query.getResultList();
    }

    public List<FlightEntity> findByAirport(final String airport) {
        LOG.info("findByAirport( {} )", airport);

        final TypedQuery<FlightEntity> query
                = entityManager.createNamedQuery(FlightEntity.FIND_BY_AIRPORT, FlightEntity.class);
        query.setParameter("departure", DateUtils.ceiling(new Date(), Calendar.DATE));
        query.setParameter("airport", airport);
        query.setMaxResults(100);
        return query.getResultList();
    }

    public List<FlightEntity> findByTarget(final String airport, final String target) {
        LOG.info("findByTarget( {}, {} )", airport, target);

        final TypedQuery<FlightEntity> query
                = entityManager.createNamedQuery(FlightEntity.FIND_BY_TARGET, FlightEntity.class);
        query.setParameter("departure", DateUtils.ceiling(new Date(), Calendar.DATE));
        query.setParameter("airport", airport);
        query.setParameter("target", target);
        query.setMaxResults(100);
        return query.getResultList();
    }
}
