package de.thaso.sws.ws.db.utils;

import de.thaso.sws.ws.db.common.DatabaseError;
import de.thaso.sws.ws.db.common.DatabaseException;
import org.hamcrest.Description;
import org.hamcrest.TypeSafeMatcher;

/**
 * DatabaseExceptionCodeMatcher
 *
 * @author thaler
 * @since 2017-12-11
 */
public class DatabaseExceptionCodeMatcher extends TypeSafeMatcher<DatabaseException> {
    private DatabaseError errorCode;

    public DatabaseExceptionCodeMatcher(DatabaseError errorCode) {
        this.errorCode = errorCode;
    }

    @Override
    protected boolean matchesSafely(final DatabaseException exception) {
        return exception.getDatabaseError() == errorCode;
    }

    public void describeTo(final Description description) {
        description.appendText("expects error code ")
                .appendValue(errorCode);
    }

    @Override
    protected void describeMismatchSafely(final DatabaseException exception,
                                          final Description mismatchDescription) {
        mismatchDescription.appendText("swa ")
                .appendValue(exception.getDatabaseError());
    }
}
