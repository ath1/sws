package de.thaso.sws.ws.db;

import de.thaso.sws.ws.db.common.DatabaseError;
import de.thaso.sws.ws.db.common.DatabaseException;
import de.thaso.sws.ws.db.utils.DatabaseExceptionCodeMatcher;
import org.apache.commons.lang3.time.DateUtils;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.hamcrest.Matchers.containsString;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;

//@RunWith(MockitoJUnitRunner.class)
public class FlightsDAOTest {

    public static final DatabaseExceptionCodeMatcher EXCEPTION_MATCHER_ENTITY_NOT_FOUND
            = new DatabaseExceptionCodeMatcher(DatabaseError.ENTITY_NOT_FOUND);

    @InjectMocks
    private FlightsDAO underTest;

    @Mock
    private EntityManager entityManager;

    @Rule
    public ExpectedException exception = ExpectedException.none();

    private Long primaryKey;
    private FlightEntity flightEntity;

    @Before
    public void setUp() {
        initMocks(this);

        primaryKey = 1L;
        flightEntity = new FlightEntity();
        when(entityManager.find(FlightEntity.class, primaryKey)).thenReturn(flightEntity);
    }

    @Test
    public void storeFlight() {
        // when
        final FlightEntity result = underTest.storeFlight(flightEntity);
        // then
        verify(entityManager).persist(flightEntity);
        assertThat(result, is(flightEntity));
    }

    @Test
    public void findFlight() {
        // when
        final FlightEntity result = underTest.findFlightById(primaryKey);
        // then
        assertThat(result, is(flightEntity));
    }

    @Test
    public void findFlight_whenPrimaryKeyNotFound() {
        // given
        when(entityManager.find(FlightEntity.class, primaryKey)).thenReturn(null);
        // when
        final FlightEntity result = underTest.findFlightById(primaryKey);
        // then
        assertThat(result, is(nullValue()));
    }

    @Test
    public void loadFlight() {
        // when
        final FlightEntity result = underTest.loadFlightById(primaryKey);
        // then
        assertThat(result, is(flightEntity));
    }

    @Test
    public void loadFlight_whenPrimaryKeyNotFound() {
        // given
        when(entityManager.find(FlightEntity.class, primaryKey)).thenReturn(null);
        exception.expect(DatabaseException.class);
        exception.expectMessage(containsString(" '" + primaryKey.toString() + "' "));
        exception.expect(EXCEPTION_MATCHER_ENTITY_NOT_FOUND);
        // when
        final FlightEntity result = underTest.loadFlightById(primaryKey);
    }

    @Test
    public void findByDate() {
        // given
        final TypedQuery query = mock(TypedQuery.class);
        when(entityManager.createNamedQuery(FlightEntity.FIND_BY_DATE,FlightEntity.class)).thenReturn(query);
        final List<FlightEntity> flightEntityList = new ArrayList<>();
        when(query.getResultList()).thenReturn(flightEntityList);
        final Date now = new Date();
        // when
        final List<FlightEntity> result = underTest.findByDate(now);
        // then
        assertThat(result,is(flightEntityList));
        verify(query).setParameter("departure", DateUtils.ceiling(now, Calendar.DATE));
        verify(query).setMaxResults(100);
    }
}