package de.thaso.sws.ws.impl.mapper;

import de.thaso.sws.ws.data.Flight;
import de.thaso.sws.ws.db.FlightEntity;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper
public interface FlightMapper {

    Flight flightToDO(FlightEntity flightEntity);

    FlightEntity flightToEntity(Flight flight);

    List<Flight> flightListToDOList(List<FlightEntity> nickNameEntityList);
}
