package de.thaso.sws.ws.impl;

import de.thaso.sws.ws.FlightsBusiness;
import de.thaso.sws.ws.data.Flight;
import de.thaso.sws.ws.db.FlightsDAO;
import de.thaso.sws.ws.impl.mapper.FlightMapper;
import org.apache.commons.lang3.RandomUtils;
import org.apache.commons.lang3.time.DateUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@ApplicationScoped
public class FlightsBusinessImpl implements FlightsBusiness {
    private static final Logger LOG = LoggerFactory.getLogger(FlightsBusiness.class);

    @Inject
    private FlightsDAO flightsDAO;

    @Inject
    private FlightMapper flightMapper;

    private Map<Long, Flight> flightStore = new HashMap<>();

    @Override
    public Flight getById(final Long id) {
        LOG.info("calling getById");
        LOG.debug("parameter getById: {}", id);

        if(id.equals(5L)) {
            return createFlightId5();
        }

        return flightMapper.flightToDO(flightsDAO.findFlightById(id));
    }

    private Flight createFlightId5() {
        final Flight flight = new Flight();

        flight.setId(5L);
        flight.setFlyno("LH6532");
        flight.setAirport("Frankfurt");
        flight.setTarget("New York");
        try {
            flight.setDeparture(DateUtils.parseDate("2018-12-18 06:45", "yyyy-MM-dd HH:mm"));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return flight;
    }

    @Override
    public void store(Flight flight) {
        LOG.info("store: {}", flight.getFlyno());

        flight.setId(RandomUtils.nextLong(1000, 10000));
        flightStore.put(flight.getId(), flight);
    }

    @Override
    public void delete(final Long id) {
        LOG.info("delete: {}", id);

        flightStore.remove(id);
    }

    @Override
    public List<Flight> loadAirport(final String airport) {
        LOG.info("loadAirport: airport '{}'", airport);

        final List<Flight> result = new ArrayList<>();
        //for(final Flight flight : flightStore.values()) {
        //    if(flight.getAirport().contains(airport)) {
        //        result.add(flight);
        //    }
        //}
        return result;
    }

    @Override
    public List<Flight> loadAirportTarget(String airport, String target) {
         LOG.info("loadAirportTarget: airport '{}' target '{}'", airport, target);

        final List<Flight> result = new ArrayList<>();
        for(final Flight flight : flightStore.values()) {
            if(flight.getAirport().contains(airport) && flight.getAirport().contains(airport)) {
                result.add(flight);
            }
        }
        return result;
    }

    private void initRandomFlightStore() {

    }
}
