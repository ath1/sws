package de.thaso.sws.ws.utils;


import de.thaso.sws.ws.data.Flight;

import java.util.Date;

public class FlightBuilder {

    private Long id;
    private String flyno;
    private Date departure;
    private String airport;
    private String target;

    public static FlightBuilder create() {
        return new FlightBuilder();
    }

    public FlightBuilder() {
    }

    public FlightBuilder withId(final Long id) {
        this.id = id;
        return this;
    }

    public FlightBuilder withNumber(final String number) {
        this.flyno = number;
        return this;
    }

    public FlightBuilder withDate(final Date date) {
        this.departure = date;
        return this;
    }

    public FlightBuilder withFrom(final String from) {
        this.airport = from;
        return this;
    }

    public FlightBuilder withTo(final String to) {
        this.target = to;
        return this;
    }

    public Flight build() {
        final Flight flight = new Flight();

        flight.setId(id);
        flight.setFlyno(flyno);
        flight.setDeparture(departure);
        flight.setAirport(airport);
        flight.setTarget(target);

        return flight;
    }
}
