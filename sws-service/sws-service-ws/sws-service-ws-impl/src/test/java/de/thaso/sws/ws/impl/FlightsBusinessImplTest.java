package de.thaso.sws.ws.impl;

import de.thaso.sws.ws.utils.FlightBuilder;
import de.thaso.sws.ws.data.Flight;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;

import static org.mockito.MockitoAnnotations.initMocks;

public class FlightsBusinessImplTest {

    public static final Long FLIGHT_ID = 5L;

    private Flight flight;

    @InjectMocks
    private FlightsBusinessImpl underTest;

    @Before
    public void setUp() throws Exception {
        initMocks(this);

        flight = FlightBuilder.create().build();
    }

    @Test
    public void getById() {
        underTest.getById(FLIGHT_ID);
    }

    @Test
    public void name() {
        underTest.store(flight);
    }
}