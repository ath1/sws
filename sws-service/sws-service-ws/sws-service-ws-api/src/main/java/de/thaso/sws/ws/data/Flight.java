package de.thaso.sws.ws.data;

import com.fasterxml.jackson.annotation.JsonFormat;

import javax.xml.bind.annotation.XmlRootElement;
import java.util.Date;

@XmlRootElement
public class Flight {

    private Long id;
    private String flyno;
    private Date departure;
    private String airport;
    private String target;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFlyno() {
        return flyno;
    }

    public void setFlyno(String flyno) {
        this.flyno = flyno;
    }

//    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ssZ")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyyMMddHHmmssz")
    public Date getDeparture() {
        return departure;
    }

    public void setDeparture(Date departure) {
        this.departure = departure;
    }

    public String getAirport() {
        return airport;
    }

    public void setAirport(String airport) {
        this.airport = airport;
    }

    public String getTarget() {
        return target;
    }

    public void setTarget(String target) {
        this.target = target;
    }

    @Override
    public String toString() {
        return "Flight{" +
                "id=" + id +
                ", flyno='" + flyno + '\'' +
                ", departure=" + departure +
                ", airport='" + airport + '\'' +
                ", target='" + target + '\'' +
                '}';
    }
}
