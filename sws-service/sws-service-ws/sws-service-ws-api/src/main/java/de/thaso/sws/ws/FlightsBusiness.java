package de.thaso.sws.ws;

import de.thaso.sws.ws.data.Flight;

import javax.enterprise.context.ApplicationScoped;
import java.util.List;

public interface FlightsBusiness {

    Flight getById(Long id);

    void store(Flight flight);

    void delete(Long id);

    List<Flight> loadAirport(String airport);

    List<Flight> loadAirportTarget(String airport, String target);
}
