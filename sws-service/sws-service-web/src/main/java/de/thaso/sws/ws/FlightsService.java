package de.thaso.sws.ws;

//import com.fasterxml.jackson.databind.ObjectMapper;
//import com.fasterxml.jackson.jaxrs.json.JacksonJaxbJsonProvider;
import de.thaso.sws.ws.data.Flight;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import java.util.List;

@ApplicationScoped
@Path("/")
public class FlightsService
{
    private static final Logger LOG = LoggerFactory.getLogger(FlightsService.class);

    @Inject
    private FlightsBusiness flightBusiness;

    @GET
    @Produces(MediaType.TEXT_PLAIN)
    @Path("ping")
    public String ping() {
        LOG.info("ping()");

        return "ok";
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("{id}")
    public Flight getById(@PathParam("id") String id) {
        LOG.info("getById({})", id);

        return flightBusiness.getById(Long.parseLong(id));
    }

    @PUT
    @Consumes(MediaType.APPLICATION_JSON)
    @Path("store")
    public void store(final Flight newFlight) {
        LOG.info("storeFlight(title = {})", newFlight != null ? newFlight.getFlyno() : "-");

        flightBusiness.store(newFlight);
    }

    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Path("loadAirport")
    public List<Flight> loadAirport(final String airport) {
        LOG.info("loadAirport({})", airport);

        return flightBusiness.loadAirport(airport);
    }
}



