package de.thaso.sws.ws.filter;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.Enumeration;

@WebFilter("/SimpleFilter")
public class SimpleFilter implements Filter {
    private static final Logger LOG = LoggerFactory.getLogger(SimpleFilter.class);

    private ServletContext context;

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
		LOG.info("SimpleFilter initialized");

        context = filterConfig.getServletContext();
    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        LOG.info("doFilter ...");

        HttpServletRequest req = (HttpServletRequest) servletRequest;
        LOG.info("contextPath: " + req.getContextPath());
        LOG.info("method: " + req.getMethod());
        logParameter(servletRequest, req, req.getParameterNames());
        logHeader(req, req.getHeaderNames());

        Cookie[] cookies = req.getCookies();
        if(cookies != null){
            for(Cookie cookie : cookies){
                LOG.info(req.getRemoteAddr() + "::Cookie::{" + cookie.getName() + "," + cookie.getValue() + "}");
            }
        }
        filterChain.doFilter(servletRequest, servletResponse);
    }

    private void logHeader(HttpServletRequest req, Enumeration<String> headerNames) {
        while (headerNames.hasMoreElements()) {
            String header = headerNames.nextElement();
            String value = req.getHeader(header);
            LOG.info(req.getRemoteAddr() + "::Request Header::{"+header+"="+value+"}");
        }
    }

    private void logParameter(ServletRequest servletRequest, HttpServletRequest req, Enumeration<String> params) {
        while(params.hasMoreElements()){
            String name = params.nextElement();
            String value = servletRequest.getParameter(name);
            LOG.info(req.getRemoteAddr() + "::Request Params::{"+name+"="+value+"}");
        }
    }

    @Override
    public void destroy() {

    }
}
