package de.thaso.sws.ws;

import de.thaso.sws.ws.data.Flight;
import org.apache.cxf.jaxrs.client.WebClient;
import org.apache.openejb.jee.WebApp;
import org.apache.openejb.junit.ApplicationComposer;
import org.apache.openejb.testing.Classes;
import org.apache.openejb.testing.EnableServices;
import org.apache.openejb.testing.Module;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;

import javax.ws.rs.core.MediaType;
import java.io.IOException;

import static org.junit.Assert.assertEquals;

@EnableServices(value = "jaxrs")
@RunWith(ApplicationComposer.class)
@Ignore
public class FlightServiceMT {

    @Module
    @Classes(FlightsService.class)
    public WebApp app() {
        return new WebApp().contextRoot("test");
    }

    @Test
    public void get() throws IOException {
        final String message = WebClient.create("http://localhost:4204").path("/test/ping").accept(MediaType.TEXT_PLAIN_TYPE).get(String.class);
        assertEquals("ok", message);
    }

    @Test
    @Ignore
    public void getFlight() throws IOException {
        final Flight message = WebClient.create("http://localhost:4204").path("/test/5").accept(MediaType.APPLICATION_JSON_TYPE).get(Flight.class);
        assertEquals("ok", message);
    }
}
