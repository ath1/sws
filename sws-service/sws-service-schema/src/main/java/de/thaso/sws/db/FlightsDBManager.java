package de.thaso.sws.db;

import de.thaso.sws.db.common.DatabaseManager;
import de.thaso.sws.db.common.PropertiesManager;

import java.util.Properties;

/**
 * SwsDBManager
 *
 * @author thaler
 * @since 2017-12-12
 */
public class FlightsDBManager {

    public static void main(final String[] args) {

        Properties properties = PropertiesManager.readDevelopProperties();

        DatabaseManager databaseManager = new DatabaseManager();
        databaseManager.migrateDatabase(properties);
    }
}
