package de.thaso.sws.ws;

//import com.fasterxml.jackson.jaxrs.json.JacksonJsonProvider;
import de.thaso.sws.ws.data.Flight;
import org.apache.commons.lang3.time.DateUtils;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.MediaType;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

public class FlightsServiceIT {
    private static final Logger LOG = LoggerFactory.getLogger(FlightsServiceIT.class);

    @Test
    public void pingTest() {

        final Client client = ClientBuilder.newClient();
        //client.register(new JacksonJsonProvider());
        final String ping = client.target("http://localhost:63278/sws/ping")
                .request(MediaType.TEXT_PLAIN)
                .get(String.class);

        LOG.info("simpleClientTest: " + ping);
        assertThat(ping, is("ok"));
    }

    @Test
    public void simpleClientTest() {

        final Client client = ClientBuilder.newClient();
        //client.register(new JacksonJsonProvider());
        final de.thaso.sws.ws.data.Flight flight = client.target("http://localhost:63278/sws/5")
                .request(MediaType.APPLICATION_JSON)
                .get(de.thaso.sws.ws.data.Flight.class);

        assertThat(flight.getFlyno(), is("LH6532"));

        LOG.info("simpleClientTest: " + flight.toString());
    }

    @Test
    public void storeFlight() {

        final Flight flight = new Flight();
        flight.setDeparture(DateUtils.addDays(DateUtils.ceiling(new Date(), Calendar.MINUTE), 5));
        flight.setAirport("Hamburg");
        flight.setTarget("New York");
        flight.setFlyno("LH26");

        storeFlight(flight);

//        loadAirport();
    }

    private void loadAirport() {
        final Client client = ClientBuilder.newClient();
        //client.register(new JacksonJsonProvider());
        final List flightList = client.target("http://localhost:63278/sws/loadAirport")
                .request(MediaType.APPLICATION_JSON)
                .post(Entity.json("Hamburg"), List.class);

        LOG.info("... " + flightList.size() + " " + flightList);
        LOG.info("... " + flightList.get(0).getClass().getSimpleName());
        for(final Flight f : (List<Flight>)flightList) {
            LOG.info("flight: " + f.toString());
        }
    }

    private void storeFlight(de.thaso.sws.ws.data.Flight flight) {
        final Client client = ClientBuilder.newClient();
        //client.register(new JacksonJsonProvider());
        client.target("http://localhost:63278/sws/store")
                .request(MediaType.APPLICATION_JSON)
                .put(Entity.json(flight));
    }
}
