package de.thaso.sws.ws.utils;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.ext.MessageBodyReader;
import javax.ws.rs.ext.Provider;
import java.io.IOException;
import java.io.InputStream;
import java.lang.annotation.Annotation;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

@Provider
public class Reader implements MessageBodyReader<List>
{

@Override
public boolean isReadable(Class<?> paramClass, Type paramType,
                          Annotation[] paramArrayOfAnnotation, MediaType paramMediaType) {
    // TODO Auto-generated method stub
    return true;
}

@Override
public List readFrom(Class<List> paramClass, Type paramType,
        Annotation[] paramArrayOfAnnotation, MediaType paramMediaType,
        MultivaluedMap<String, String> paramMultivaluedMap,
        InputStream paramInputStream) throws IOException,
        WebApplicationException {
    // TODO Auto-generated method stub

    return new ArrayList();
}

}